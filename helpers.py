#!/usr/bin/env python3
# encoding: utf-8
"""Helper functions for the bench workshop."""

import numpy as np
import matplotlib.pyplot as plt
import nglview

def view_ngl(atoms):
    """View `atoms` with the NGLViewer library."""
    if isinstance(atoms, list):
        view = nglview.show_asetraj(atoms)
    else:
        view = nglview.show_ase(atoms)

    view._remote_call('setSize', target='Widget', args=['400px', '200px'])
    view.parameters = dict(
        backgroundColor='white',
        clip_dist=-1,
        clip_far=100,
        sampleLevel=3
    )

    view.control.zoom(0.7)
    view.player.widget_player._repeat = True
    view.player.widget_player.interval = 10       

    return view

def plot_energy(dataset, indices=None):
    """Plot the distribution of energies in `dataset`.

    If `indices` is given, only the structures at `indices` in `dataset`
    will be plotted.
    """
    energies = np.array([i.calc.results['energy'] for i in dataset])
    n_atoms = np.array([len(i) for i in dataset])
    
    if indices is None:
        indices = np.arange(0, len(energies), 1)
        
    plt.scatter(indices, energies / n_atoms, s=2)

    plt.title('Energy Distribution in the Training Dataset')
    plt.xlabel('Structure Index')
    plt.ylabel('Energy $E$ / eV atom$^{-1}$')
